---
- name: Configure global settings
  ansible.builtin.template:
    src: include_conf.j2
    dest: "/etc/postgresql/{{ postgresql_version }}/main/conf.d/include.conf"
    owner: postgres
    group: postgres
    mode: 0644
  when: postgresql_conf is defined
  notify: pg_reload_conf
  register: postgresql_conf_change
  tags:
    - postgresql_conf

- name: Notify
  ansible.builtin.debug:
    msg: "Configure global settings: this task may require a restart of the database service."
  when: postgresql_conf_change.changed

- name: Create users
  become_user: postgres
  no_log: true
  community.postgresql.postgresql_user:
    encrypted: true
    name: "{{ item.name }}"
    password: "{{ item.password | default(omit) }}"
    role_attr_flags: "{{ item.role_attr_flags }}"
    state: "{{ item.state | default('present') }}"
  with_items:
    - "{{ postgresql_user }}"
  when: postgresql_user is defined
  tags:
    - postgresql_user

- name: Create database
  become_user: postgres
  community.postgresql.postgresql_db:
    name: "{{ item.name }}"
    owner: "{{ item.owner | default(omit) }}"
    state: "{{ item.state | default('present') }}"
    tablespace: "{{ item.tablespace | default(omit) }}"
  with_items: "{{ postgresql_db }}"
  when: postgresql_db is defined
  tags:
    - postgresql_db

- name: Adds extensions
  become_user: postgres
  community.postgresql.postgresql_ext:
    name: "{{ item.name }}"
    db: "{{ item.db }}"
  with_items: "{{ postgresql_ext }}"
  when: postgresql_ext is defined
  tags:
    - postgresql_ext

- name: Create schema
  become_user: postgres
  community.postgresql.postgresql_schema:
    name: "{{ item.name }}"
    db: "{{ item.db }}"
  with_items: "{{ postgresql_schema }}"
  when: postgresql_schema is defined
  tags:
    - postgresql_schema

- name: Set privileges
  become_user: postgres
  community.postgresql.postgresql_privs:
    db: "{{ item.db }}"
    state: "{{ item.state | default('present') }}"
    schema: "{{ item.schema | default(omit) }}"
    privs: "{{ item.privs | default(omit) }}"
    type: "{{ item.type | default(omit) }}"
    roles: "{{ item.roles }}"
    objs: "{{ item.objs | default(omit) }}"
    target_roles: "{{ item.target_roles | default(omit) }}"
  with_items: "{{ postgresql_privs }}"
  when: postgresql_privs is defined
  tags:
    - postgresql_privs

- name: Configure pg_hba, postgres version lower than 16
  become_user: postgres
  postgresql_pg_hba:
    dest: "{{ item.dest | default('/etc/postgresql/' ~ postgresql_version ~ '/main/pg_hba.conf') }}"
    state: "{{ item.state | default('present') }}"
    contype: "{{ item.contype }}"
    users: "{{ item.users }}"
    source: "{{ item.source }}"
    databases: "{{ item.databases }}"
    method: "{{ item.method }}"
    comment: "{{ item.comment | default(omit) }}"
    keep_comments_at_rules: true
    backup_file: true
  with_items: "{{ postgresql_pg_hba }}"
  when:
    - postgresql_pg_hba is defined
    - postgresql_version|int < 16
  notify: pg_reload_conf
  tags:
    - postgresql_pg_hba

- name: Configure pg_hba.conf and pg_hba_custom.conf, postgres version 16 and higher
  block:
    - name: Add include directive to pg_hba.conf
      become_user: postgres
      lineinfile:
        path: "/etc/postgresql/{{ postgresql_version }}/main/pg_hba.conf"
        line: "include pg_hba_custom.conf"
    - name: Create custom pg_hba file
      become_user: postgres
      community.postgresql.postgresql_pg_hba:
        dest: "/etc/postgresql/{{ postgresql_version }}/main/pg_hba_custom.conf"
        overwrite: true
        create: true
        keep_comments_at_rules: true
        rules: "{{ postgresql_pg_hba }}"
  when:
    - postgresql_pg_hba is defined
    - postgresql_version|int >= 16
  notify: pg_reload_conf
  tags:
    - postgresql_pg_hba

- name: Configure membership
  become_user: postgres
  community.postgresql.postgresql_membership:
    db: "{{ item.db | default(omit) }}"
    group: "{{ item.group }}"
    target_roles: "{{ item.target_roles }}"
    state: "{{ item.state | default('present') }}"
  with_items: "{{ postgresql_membership }}"
  when: postgresql_membership is defined
  tags:
    - postgresql_membership

- name: Run queries
  become_user: postgres
  community.postgresql.postgresql_query:
    db: "{{ item.db }}"
    query: "{{ item.query }}"
  with_items: "{{ postgresql_query }}"
  when: postgresql_query is defined
  tags:
    - postgresql_query
...
