ansible-role-postgresql
=========

Installs and configures PostgreSQL server on Ubuntu servers. Also playbooks to create a slave database, create a backup, move the postgresql datadirectory and create systemd unit file to schedule backups using systemd timers.

# Requirements
------------
- Ansible 2.8 or higher
- Ubuntu 18.04, 20.04 or 22.04 on target hosts


# Role Variables
--------------
```yaml
# Version to install
postgresql_version: 16

# Install PostGIS package, default false
install_postgis: false

# Used by playbook `move_datadir` playbook to move data dir to alternative location using symlink
postgresql_dir: /data

# Configuration settings, using include directive, settings are saved in /etc/postgresql/.../main/conf.d/include.conf
postgresql_conf:
  listen_addresses: "'*'"

# Create users
postgresql_user:
  - name: analytics
    password: password
    role_attr_flags: LOGIN
  - name: replica_user
    password: password
    role_attr_flags: LOGIN
  - name: super_user
    password: password
    role_attr_flags: SUPERUSER,CREATEROLE,CREATEDB,REPLICATION
  - name: readonly
    role_attr_flags: NOLOGIN
  - name: foo
    password: bar
    role_attr_flags: LOGIN

# Create database
postgresql_db:
  - name: db1
    owner: postgres

# Create schema:
postgresql_schema:
  - db: db1
    name: myschema

# Create extensions
postgresql_ext:
  - db: db1
    name: pg_stat_statements

# Create privs
postgresql_privs:
  # REVOKE CREATE ON SCHEMA public FROM PUBLIC, not needed anymore from psql 15 onwards
  - db: db1
    roles: PUBLIC
    privs: CREATE
    type: schema
    objs: public
    state: absent
  # GRANT CONNECT ON DATABASE db1 TO readonly:
  - db: db1
    roles: readonly
    privs: CONNECT
    type: database
  # GRANT USAGE ON SCHEMA public TO readonly:
  - db: db1
    roles: readonly
    privs: USAGE
    type: schema
    objs: public
  # GRANT SELECT ON ALL TABLES IN SCHEMA public TO readonly:
  - db: db1
    roles: readonly
    privs: SELECT
    schema: public
    type: table
    objs: ALL_IN_SCHEMA
  # ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT SELECT ON TABLES TO readonly:
  # ALTER DEFAULT PRIVILEGES FOR ROLE super_user IN SCHEMA public GRANT SELECT ON TABLES TO readonly:
  # ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT SELECT ON SEQUENCES TO readonly:
  # ALTER DEFAULT PRIVILEGES FOR ROLE super_user IN SCHEMA public GRANT SELECT ON SEQUENCES TO readonly:
  - db: db1
    roles: readonly
    objs: TABLES,SEQUENCES
    privs: SELECT
    type: default_privs
    target_roles: postgres,super_user

# Run queries
postgresql_query:

# Set pg_hba rules
postgresql_pg_hba:
  - contype: host
    users: foo
    source: 10.119.0.0/16
    databases: all
    method: scram-sha-256
    comment: "My comment"

# Manage memberships
postgresql_membership:
  # GRANT pg_monitor TO analytics:
  - group: pg_monitor
    target_roles: analytics
  # GRANT readonly TO foo:
  - group: readonly
    target_roles: foo

# Creates a backup script and a corresponding systemd service to automate the backup process for all databases
postgresql_systemd_backup_service: true
```

# Dependencies
------------
None.

# Example Playbook
----------------
```yaml
- name: Install and manage PostgreSQL
  hosts: postgresql
  become: true
  gather_facts: true
  tasks:
    - name: Apply postgres role
      include_role:
        name: postgresql
        apply:
          tags: postgresql
      tags: always

    - name: Create dumps
      include_role:
        name: postgresql
        tasks_from: create_dumps.yml
        apply:
          tags: create_dumps
      tags: always

    - name: Create slave
      include_role:
        name: postgresql
        tasks_from: create_slave.yml
        apply:
          tags: create_slave
      tags: always
```
# Example ansible-playbook command to only install PostgreSQL
-----------------------------------------------------------
```
ansible-playbook -l yourhost -t postgresql_install
```

## Example ansible-playbook command to only create PostgreSQL users
----------------------------------------------------------------
```
ansible-playbook -l yourhost -t postgresql_user
```

# Links
_______
* https://pgtune.leopard.in.ua/ to finetune your postgres configuration depending on the given resources and demands.

# Author Information
------------------
Rudi Broekhuizen
